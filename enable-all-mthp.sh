#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Enable all mTHP sizes
echo "always" > /sys/kernel/mm/transparent_hugepage/enabled || { echo 'enabling THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-16kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-32kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-64kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-128kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-256kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-512kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-1024kB/enabled || { echo 'configuring THP failed' ; exit 1; }
echo "inherit" > /sys/kernel/mm/transparent_hugepage/hugepages-2048kB/enabled || { echo 'configuring THP failed' ; exit 1; }

echo "All mTHP sizes successfully enabled"
