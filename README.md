# Paper Artifacts

These are the software artifacts for the paper
"Every Mapping Counts in Large Amounts: Folio Accounting". They include patches for two custom Linux kernels based on Linux 6.7, three microbenchmarks, two macrobenchmarks and helper scripts to install and run the artifacts.

The artifacts are prepared to be run on an x86-64 machine with root access, 20 cores, 32 GiB of RAM and 30 GiB of free disk space. Further, the provided scripts assume that Fedora 38, 39 or 40 is installed. Running these artifacts on other machines or under other Linux distributions might require adaptions to installation and run scripts.

All commands are expected to be run by a Linux user with sudo (root) permissions. Internet access is required to download software packages and Linux 6.7 source code. It is recommended to use a freshly installed system that can be modified by the scripts, avoiding any chance for data loss.

To ensure accuracy, Hyper-Threading should be disabled and the CPU frequency should be fixed (e.g., disabling Turbo-Boost, enabling the "performance" CPU governor, configuring a fixed CPU frequency).

## Getting Started Instructions

First, compile and install the custom Linux kernels. Then, switch into the __6.7.0-foliomap__ kernel and make sure basic functionality is working.

### Installing Custom Linux Kernels

The __install.sh__ script in the *kernels* directory will automatically install required packages using the DNF package manager, download Linux 6.7, and compile and install the two custom Linux kernels. Note that this script will also install some packages required for compiling and running microbenchmarks later and that compilation of two custom Linux kernels can take some time.

```
$ cd kernels
$ sudo ./install.sh
```

If everything went as expected, the two custom Linux kernels should be visible as possible boot options. __grubby__ can list the installed kernels.

```
$ sudo grubby --info=ALL
```

There should be entries referencing kernels containing the string __6.7.0-baseline__ and __6.7.0-foliomap__.

### Switching Between Custom Linux Kernels

To run the experiments on the two custom Linux kernels, reboots are required. To minimize users errors, all benchmarks store results into directories named after the currently booted Linux kernel.

While selecting a custom Linux kernel to boot in the boot manager (GRUB) is one approach, the suggestion is to configure the next kernel to boot from Linux directly.

After installing the custom Linux kernels, observe all installed
kernels via the __grubby__ command:

```
$ sudo grubby --info=ALL
```

Select a kernel by noting the "index=" value and supply it to the
__grubby__ command, then reboot. Assuming “index=1”:

```
$ sudo grubby --set-default-index=1
$ sudo shutdown −r now
```

The relationship between the index assignment and the custom Linux kernels might be as follows:

| Index | Kernel Name    | Approach |
|-------|----------------|----------|
| 0     | 6.7.0-foliomap | FolioMap |
| 1     | 6.7.0-baseline | Baseline |

After the reboot, verify the correct kernel was booted using the __uname__ command. Assuming "index=0" corresponds to 6.7.0-foliomap:

```
$ uname −r
6.7.0-foliomap
```

### Testing Basic Functionality ("Hello World"-sized Example)

Switch into the __6.7.0-foliomap__ kernel. Linux might now already make use of mTHP (large folios) in the pagecache if supported by the filesystem. Note that all large folios (pagecache and anonymous memory) that are mapped into user space processes use the same folio accounting approach: FolioMap under the __6.7.0-foliomap__ kernel.

To enable all mTHP (large folios) sizes for anonymous memory globally in the system, the helper script __enable-all-mthp.sh__  in the artifacts root directory can be used:

```
$ sudo ./enable-all-mthp.sh 
All mTHP sizes successfully enabled
```

Running any application will now make use of mTHP for anonymous memory if applicable. Note that benchmark scripts will override that configuration.

To briefly test if the COW reuse logic for mTHP is working as expected, execute the "write-fault-latency" microbenchmark, and verify that the "reuse" times are similar across folio sizes and are always significantly smaller than the "cow" times. For example:

```
$ cd benchmarks
$ sudo ./run-write-fault-latency.sh > /dev/null
$ cat results/write-fault-latency/6.7.0-foliomap/reuse.csv 
FolioSize(KiB),Time(s),RSD
4,686.006469,.085567
16,661.599205,.081635
32,655.803968,.062301
64,658.299598,.096416
128,656.012323,.076529
256,657.440380,.076811
512,664.848526,.099135
1024,658.855255,.082620
2048,660.103303,.081464
$ cat results/write-fault-latency/6.7.0-foliomap/cow.csv 
FolioSize(KiB),Time(s),RSD
4,1973.861803,.088329
16,2049.921905,.095450
32,1994.260609,.092676
64,1938.677681,.070520
128,1956.612792,.075888
256,1931.295421,.071148
512,1917.061840,.057600
1024,1925.549411,.062243
2048,1929.733809,.055249
```

## Detailed Instructions

Delete the *benchmarks/results* directory to remove any results from any previous benchmark runs while "Testing Basic Functionality". Execute all benchmarks for both custom Linux kernel. Rebooting after running each benchmark is recommended for accurate results.

### "OS Primitives" Benchmark

Compile and run the benchmark using the __run-pte-mapped-folio-benchmarks.sh__ script. This will execute the *fork* and *munmap* benchmarks for each available folio size, one size at a time.

```
$ cd benchmarks
$ sudo ./run-pte-mapped-folio-benchmarks.sh
```

The results will be stored into a sub-directory in
*benchmarks/results/pte-mapped* that corresponds to the current Linux kernel, e.g., into *benchmarks/results/pte-mapped/6.7.0-foliomap*.

Note that if anything goes wrong while running the benchmark, the relevant sub-directory in *benchmarks/results/pte-mapped* must be deleted to re-run the benchmark.

The __run-pte-mapped-folio-benchmarks.sh__ script will automatically process the results and output two additional CSV files in the result sub-directory for each Linux kernel:

* *fork.csv*: Average time in seconds per folio size when running the *fork* microbenchmark. These results correspond to the results depicted in Figure 3a in the paper.
* *munmap.csv*: Average time in seconds per folio size when running the *munmap* microbenchmark. These results correspond to the results depicted in Figure 3b in the paper.

### "Write Fault Latency with COW" Benchmark

Compile and run the benchmark using the __run-write-fault-latency.sh__ script. This script will execute the *COW* and *reuse* benchmarks for each available folio size, one size at a time.

```
$ cd benchmarks
$ sudo ./run-write-fault-latency.sh
```

The results will be stored into a sub-directory in
*benchmarks/results/write-fault-latency* that corresponds to the current Linux kernel, e.g., into *benchmarks/results/write-fault-latency/6.7.0-foliomap*.

Note that if anything goes wrong while running the benchmark, the relevant sub-directory in *benchmarks/results/write-fault-latency* must be deleted to re-run the benchmark.

The __run-write-fault-latency.sh__ script will automatically process the results and output two additional CSV files in the result sub-directory for a Linux kernel:

* *reuse.csv*: Average time in nanoseconds per folio size when running the *reuse* microbenchmark. These results correspond to the results mentioned in Section 4.1 in the paper when COW operations are not required because the child process no longer maps the folios.
* *cow.csv*: Average time in nanoseconds per folio size when running the *cow* microbenchmark. These results correspond to the results mentioned in Section 4.1 in the paper when COW operations are required because the child process still maps the folios.

### "Concurrent Accounting Operations" Benchmark

Compile and run the vm-scalability benchmark using the __run-vm-scalability.sh__ script. This will execute the *anon-cow-seq* and *anon-cow-rand* microbenchmarks for each available folio size, one size at a time.

```
$ cd benchmarks
$ sudo ./run-vm-scalability.sh
```

Note that this benchmark will execute one process per CPU, consuming 1 GiB of memory per process. Consequently, a system with 20 CPUs will execute 20 processes, consuming at least 20 GiB. If required, the number of processes can be adjusted in the __run-vm-scalability.sh__ script by modifying the *TASKS* variable.

The results will be stored into a sub-directory in
*benchmarks/results/vm-scalability* that corresponds to the current Linux kernel, e.g., into *benchmarks/results/vm-scalability/6.7.0-foliomap*.

Note that if anything goes wrong while running the benchmark, the relevant sub-directory in *benchmarks/results/vm-scalability* must be deleted to re-run the benchmark.

The __run-vm-scalability.sh__ script will automatically process the results and output two additional CSV files in the result sub-directory for a Linux kernel:

* *case-anon-cow-seq.csv*: Average throughput in GiB/s per folio size. These results correspond to the results in Section 4.1 in the paper when running the *case-anon-cow-seq* microbenchmark.
* *case-anon-cow-rand-csv*: Average throughput in GiB/s per folio size. These results correspond to the results in Section 4.1 in the paper when running the *case-anon-cow-rand* microbenchmark.

### "Redis with Snapshotting" Benchmark

Running the Redis benchmark will consume around 10 GiB of RAM and up to 20 GiB of disk storage. The script will perform 10 benchmark runs, with 4 KiB and 64 KiB folio size each. Consequently, running this benchmark can take a longer time.

*To reproduce the IOPS result as shown in the upper sub-figure of Figure 4 in the paper, a system with two sockets (10 cores and 16 GiB of RAM per socket) as used for the evaluation in the paper might be required.*

First, install Redis, and compile+install the memtier_benchmark using the __install-memtier_benchmark.sh__ script.

```
$ cd benchmarks
$ sudo ./install-memtier_benchmark.sh
```

Next, copy the prepared *redis.conf* configuration file to */etc/redis/*, overwriting the original one. This configuration was adjusted to allow for THP, to disable RDB compression, and to disable automatic saving of snapshots.

```
$ sudo cp redis.conf /etc/redis/
```

It might be required to adjust the systemd Redis startup timeout, otherwise loading the database during startup might fail. Under Fedora, this timeout is configured in */etc/systemd/system/redis.service.d/limit.conf*:

```
$ sudo sed '/TimeoutStartSec/d' -i /etc/systemd/system/redis.service.d/limit.conf
$ echo "TimeoutStartSec=900s" | sudo tee -a /etc/systemd/system/redis.service.d/limit.conf
$ sudo systemctl daemon-reload
```

Run the memtier_benchmark using the __run-memtier_benchmark.sh__ script. Before the first benchmark run, the script will initialize the database, which can take up to 10 minutes.

```
$ sudo ./run-memtier_benchmark.sh
```

The results will be stored into a sub-directory in
*benchmarks/results/memtier* that corresponds to the current Linux kernel, e.g., into *benchmarks/results/memtier/6.7.0-foliomap*.

Note that if anything goes wrong while running the benchmark, the relevant sub-directory in *benchmarks/results/memtier* must be deleted to re-run the benchmark.

Within the sub-directory for each Linux kernel will be 10 sub-directories, corresponding to results for 10 benchmark iterations, e.g., *benchmarks/results/memtier/6.7.0-foliomap/1* for iteration 1. The __run-memtier_benchmark.sh__ script will not average the results across iterations, this step must be done manually.

Within each iteration sub-directory, the following files are relevant regarding the results presented in the paper:

* *4KiB/fork_usec*: The *fork* invocation time in milliseconds mentioned in Section 4.1 when running the benchmark with 4 KiB folio size.
* *64KiB/fork_usec*: Same as *4KiB/fork_usec*, but using 64 KiB folio size instead.
* *4KiB/results.csv*: The "CurOps/s" column contains the monitored IOPS per second, as shown in the upper sub-figure of Figure 4 in the paper.
* *64KiB/results.csv*: Same as *4KiB/results.csv*, but using 64 KiB folio size instead.
* *4KiB/copies.csv*: The "Copies/s" column contains the monitored COW operations per second, as shown in the lower sub-figure of Figure 4 in the paper.
* *64KiB/copies.csv*: Same as *4KiB/copies.csv*, but using 64 KiB folio size instead.

*Note: For more accurate results, on a system with multiple NUMA nodes it is recommended to effectively disable all but a single NUMA node by "offlining" all CPUs belonging to other NUMA nodes.*

### "Python Sharing Memory with Subprocesses" Benchmark

Run the Python benchmark using the __run-python-benchmark.sh__ script. This script will first install required Python packages using the DNF package manager to then execute the benchmark for each available folio size, one size at a time.

```
$ cd benchmarks
$ sudo ./run-python-benchmark.sh
```

The results will be stored into a sub-directory in
*benchmarks/results/python-benchmark* that corresponds to the current Linux kernel, e.g., into *benchmarks/results/python-benchmark/6.7.0-foliomap*.

Note that if anything goes wrong while running the benchmark, the relevant sub-directory in *benchmarks/results/python-benchmark* must be deleted to re-run the benchmark.

The __run-python-benchmark.sh__ script will automatically process the results and output a *results.csv* CSV file in the result sub-directory for a Linux kernel.

This CSV file contains the average measured time in seconds and the average measured system-wide number of COW operations. These results correspond to the results shown in Figure 5 in the paper.

*Note: For more accurate results, on a system with multiple NUMA nodes it is recommended to effectively disable all but a single NUMA node by "offlining" all CPUs belonging to other NUMA nodes.*

### Extracting Results

After running all experiments on both custom Linux kernels, the helper script __summary.py__ can be used to extract and summarize the results.

```
$ cd benchmarks
$ sudo python summary.py
```
