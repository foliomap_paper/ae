From 472b2a15b6245d5932ee00974b983f3ab029f579 Mon Sep 17 00:00:00 2001
From: Ryan Roberts <ryan.roberts@arm.com>
Date: Mon, 11 Dec 2023 12:53:20 +0000
Subject: [PATCH 04/75] mm: thp: fix build warning when CONFIG_SYSFS is
 disabled

huge_anon_orders_lock is used only to serialize sysfs writers.  So move
its definition so that it is within the CONFIG_SYSFS ifdefery to suppress
"defined but not used" warning when sysfs is disabled.

Link: https://lkml.kernel.org/r/20231211125320.3997543-1-ryan.roberts@arm.com
Fixes: ("mm: thp: Introduce multi-size THP sysfs interface")
Signed-off-by: Ryan Roberts <ryan.roberts@arm.com>
Reported-by: kernel test robot <lkp@intel.com>
Closes: https://lore.kernel.org/oe-kbuild-all/202312111916.YbsHxKPq-lkp@intel.com/
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
(cherry picked from commit ebcfd6923f6928ba6a82298d891b8f07fb354af0)
Signed-off-by: David Hildenbrand <david@redhat.com>
---
 mm/huge_memory.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/mm/huge_memory.c b/mm/huge_memory.c
index b06f1a5d2925..ec1fc0efbd9e 100644
--- a/mm/huge_memory.c
+++ b/mm/huge_memory.c
@@ -77,7 +77,6 @@ unsigned long huge_zero_pfn __read_mostly = ~0UL;
 unsigned long huge_anon_orders_always __read_mostly;
 unsigned long huge_anon_orders_madvise __read_mostly;
 unsigned long huge_anon_orders_inherit __read_mostly;
-static DEFINE_SPINLOCK(huge_anon_orders_lock);
 
 unsigned long __thp_vma_allowable_orders(struct vm_area_struct *vma,
 					 unsigned long vm_flags, bool smaps,
@@ -442,6 +441,7 @@ static const struct attribute_group hugepage_attr_group = {
 
 static void hugepage_exit_sysfs(struct kobject *hugepage_kobj);
 static void thpsize_release(struct kobject *kobj);
+static DEFINE_SPINLOCK(huge_anon_orders_lock);
 static LIST_HEAD(thpsize_list);
 
 struct thpsize {
-- 
2.45.0

