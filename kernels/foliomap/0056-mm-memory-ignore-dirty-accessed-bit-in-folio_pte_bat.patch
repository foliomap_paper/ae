From f29f737d30b8c8374f8811e43fdc0d19a9b2f635 Mon Sep 17 00:00:00 2001
From: David Hildenbrand <david@redhat.com>
Date: Sat, 30 Dec 2023 12:54:35 +0100
Subject: [PATCH 56/75] mm/memory: ignore dirty/accessed bit in
 folio_pte_batch()

Let's ignore these values, they are irrelevant for the current caller
and for the upcoming caller.

Signed-off-by: David Hildenbrand <david@redhat.com>
---
 mm/memory.c | 6 +++---
 1 file changed, 3 insertions(+), 3 deletions(-)

diff --git a/mm/memory.c b/mm/memory.c
index d5eb61d19bb2..fbf34175ef34 100644
--- a/mm/memory.c
+++ b/mm/memory.c
@@ -954,18 +954,18 @@ static __always_inline void __copy_present_ptes(struct vm_area_struct *dst_vma,
 /*
  * Detect PTEs that map consecutive pages of the same folio.
  *
- * All PTE bits have to match.
+ * All PTE bits have to match, except accessed/dirty.
  */
 static inline int folio_pte_batch(struct folio *folio, unsigned long addr,
 		pte_t *start_ptep, pte_t pte, int max_nr)
 {
 	unsigned long folio_end_pfn = folio_pfn(folio) + folio_nr_pages(folio);
 	const pte_t *end_ptep = start_ptep + max_nr;
-	pte_t expected_pte = pte_next_pfn(pte);
+	pte_t expected_pte = pte_mkclean(pte_mkold(pte_next_pfn(pte)));
 	pte_t *ptep = start_ptep + 1;
 
 	while (ptep != end_ptep) {
-		pte = ptep_get(ptep);
+		pte = pte_mkclean(pte_mkold(ptep_get(ptep)));
 
 		/* Do all PTE bits match, and the PFN is consecutive? */
 		if (!pte_same(pte, expected_pte))
-- 
2.45.0

