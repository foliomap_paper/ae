#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Install required packages
dnf install -y vim gcc patch make wget git bc tar openssl-devel openssl flex bison elfutils-libelf-devel zstd numactl numactl-devel libnsl gcc-gfortran g++ || { echo 'installing packages failed' ; exit 1; }

# Update some packages that are problematic in stock Fedora 39
dnf update -y grubby grub2 systemd-udev || { echo 'installing packages failed' ; exit 1; }

# Download Linux v6.7
if [ ! -f "linux-6.7.tar.gz" ]; then
	echo "Downloading Linux 6.7"
	wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.7.tar.gz || { echo 'download failed' ; exit 1; }
fi

# Compile and install the individual custom kernels
KERNELS="baseline foliomap"
for K in $KERNELS; do
        echo "Processing kernel $K"

        # extract and copy all patches over
        tar xzf linux-6.7.tar.gz > /dev/null
        cp $K/*.patch linux-6.7 > /dev/null

        # apply all patches
        pushd linux-6.7
        for F in *.patch; do
                patch -p1 -i $F > /dev/null
        done

        # compile and install the kernels
        make -j20  > /dev/null 2>&1 || { echo 'compilation failed' ; exit 1; }
        make modules_install > /dev/null 2>&1 || { echo 'module installation failed' ; exit 1; }
        make install > /dev/null || { echo 'installation failed' ; exit 1; }
        popd

        # cleanup
        rm -rf linux-6.7
done
