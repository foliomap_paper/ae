import csv

KERNELS = ["6.7.0-foliomap", "6.7.0-baseline"]

print("OS Primitives")
for op in ["fork", "munmap"]:
    print(op)
    for kernel in KERNELS:
        print(kernel, end="  ")
        with open(f"results/pte-mapped/{kernel}/{op}.csv", "r") as fcsv:
            reader = csv.DictReader(fcsv)
            for row in reader:
                print(
                    f"  {row['FolioSize(KiB)']}: {float(row['Time(s)'])*1000.0:5.2f}",
                    end="",
                )
        print()
print()

print("Write Fault Latency with COW")
for scene in ["reuse", "cow"]:
    print(scene)
    for kernel in KERNELS:
        print(kernel, end="  ")
        with open(f"results/write-fault-latency/{kernel}/{scene}.csv", "r") as fcsv:
            reader = csv.DictReader(fcsv)
            for row in reader:
                print(
                    f"  {row['FolioSize(KiB)']}: {float(row['Time(ns)']):7.2f}",
                    end="",
                )
        print()
print()

print("Concurrent Accounting Operations")
for op in ["seq", "rand"]:
    print(op)
    for kernel in KERNELS:
        print(kernel, end="  ")
        with open(
            f"results/vm-scalability/{kernel}/case-anon-cow-{op}.csv", "r"
        ) as fcsv:
            reader = csv.DictReader(fcsv)
            for row in reader:
                print(
                    f"  {row['FolioSize(KiB)']}: {float(row['Throughput(GB/s)']):5.2f}",
                    end="",
                )
        print()
print()

print("Redis with Snapshotting")
print("fork_usec")
for size in ["4KiB", "64KiB"]:
    print(size)
    for kernel in KERNELS:
        print(kernel)
        for i in range(1, 11):
            print(f"{i:2d}", end="  ")
            with open(f"results/memtier/{kernel}/{i}/{size}/fork_usec", "r") as fusec:
                print(f"  {float(fusec.readline()):6.2f}", end="")
            print()
for metric in [("results", "CurOps/s"), ("copies", "Copies/s")]:
    print(metric[1])
    for size in ["4KiB", "64KiB"]:
        print(size)
        for kernel in KERNELS:
            print(kernel)
            for i in range(1, 11):
                print(f"{i:2d}", end="  ")
                with open(
                    f"results/memtier/{kernel}/{i}/{size}/{metric[0]}.csv", "r"
                ) as fcsv:
                    reader = csv.DictReader(fcsv)
                    for row in reader:
                        print(f" {float(row[metric[1]])/1000.0:4.0f}", end="")
                print()
print()

print("Python Sharing Memory with Subprocesses")
for metric in ["Time(s)", "Copies"]:
    print(metric)
    for kernel in KERNELS:
        print(kernel, end="  ")
        with open(f"results/python-benchmark/{kernel}/results.csv", "r") as fcsv:
            reader = csv.DictReader(fcsv)
            for row in reader:
                print(
                    f"  {row['FolioSize(KiB)']}: {float(row[metric])/(1000.0 if metric == 'Copies' else 1):6.2f}",
                    end="",
                )
        print()
print()
