#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# we need python3
echo "Installing python3"
dnf install -y python3 numpy || { echo 'installation failed' ; exit 1; }

./prepare.sh

KERNEL=`uname -r`
DIR="results/python-benchmark/$KERNEL"
SIZES="4 16 32 64 128 256 512 1024 2048"

if [ -d "$DIR" ]; then
	echo "$DIR already exist"
	exit 1
fi
mkdir -p $DIR

echo "FolioSize(KiB),Time(s),RSD,Reuses,Reuses RSD,Copies,Copies RSD" > $DIR/results.csv

for s in $SIZES; do

	echo "Processing $s KiB"

	if [[ "$s" != "4" ]]; then
		echo always > /sys/kernel/mm/transparent_hugepage/hugepages-${s}kB/enabled || { echo 'enabling THP failed' ; exit 1; }
	fi

	echo "Time(s),Reuses,Copies" > $DIR/$s.csv

	# dry run
	python3 python-benchmark.py
	python3 python-benchmark.py
	python3 python-benchmark.py

	for i in $(seq 1 1 10); do
		OLDR=`cat /proc/vmstat | grep pgreuse | cut -d" " -f2`
		OLDC=`cat /proc/vmstat | grep pgcopy | cut -d" " -f2`
		
		start=`date +%s.%N`
		python3 python-benchmark.py
		end=`date +%s.%N`
		
		NEWR=`cat /proc/vmstat | grep pgreuse | cut -d" " -f2`
		NEWC=`cat /proc/vmstat | grep pgcopy | cut -d" " -f2`
		
		DIFFR=$((NEWR - OLDR))
		DIFFC=$((NEWC - OLDC))
		runtime=$( echo "$end - $start" | bc -l )
	
		echo "$runtime,$DIFFR,$DIFFC" >> $DIR/$s.csv
	done

	if [[ "$s" != "4" ]]; then
		echo never > /sys/kernel/mm/transparent_hugepage/hugepages-${s}kB/enabled || { echo 'disabling THP failed' ; exit 1; }
	fi

	TIMES=`tail -n +2 $DIR/$s.csv | cut -d "," -f1`
	REUSES=`tail -n +2 $DIR/$s.csv | cut -d "," -f2`
	COPIES=`tail -n +2 $DIR/$s.csv | cut -d "," -f3`

	sd=$(
		echo "$TIMES" |
			awk '{sum+=$1; sumsq+=$1*$1}END{printf "%.6f", sqrt(sumsq/NR - (sum/NR)**2) }'
	)
	sdr=$(
		echo "$REUSES" |
			awk '{sum+=$1; sumsq+=$1*$1}END{printf "%.6f", sqrt(sumsq/NR - (sum/NR)**2) }'
	)
	sdc=$(
		echo "$COPIES" |
			awk '{sum+=$1; sumsq+=$1*$1}END{printf "%.6f", sqrt(sumsq/NR - (sum/NR)**2) }'
	)
	avg=$(
		echo "$TIMES" |
			awk '{ total += $1; count++ } END { printf "%.6f", total/count }'
	)
	avgr=$(
		echo "$REUSES" |
			awk '{ total += $1; count++ } END { printf "%.6f", total/count }'
	)
	avgc=$(
		echo "$COPIES" |
			awk '{ total += $1; count++ } END { printf "%.6f", total/count }'
	)
	rsd=`echo "scale=6 ; $sd / $avg" | bc`
	rsdr=`echo "scale=6 ; $sdr / $avgr" | bc`
	rsdc=`echo "scale=6 ; $sdc / $avgc" | bc`


	echo "$s,$avg,$rsd,$avgr,$rsdr,$avgc,$rsdc" >> $DIR/results.csv
done
