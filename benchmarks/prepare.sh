#!/bin/bash

# Desired by redis
sysctl vm.overcommit_memory=1 >/dev/null 2>&1

# Disable NUMA balancing just in case
echo 0 > /proc/sys/kernel/numa_balancing

# Disable ASLR
echo 0 > /proc/sys/kernel/randomize_va_space

# We want maximum power!
#echo 0 > /sys/devices/system/cpu/cpufreq/boost
#cpupower frequency-set -g performance
#cpupower frequency-set -d 2400mhz

# Start with a clean slate
echo "never" > /sys/kernel/mm/transparent_hugepage/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-16kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-32kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-64kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-128kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-256kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-512kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-1024kB/enabled
echo "never" > /sys/kernel/mm/transparent_hugepage/hugepages-2048kB/enabled

# Flush caches before each benchmark
sync
echo 3 > /proc/sys/vm/drop_caches
