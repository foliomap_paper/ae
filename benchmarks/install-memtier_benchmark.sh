#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

dnf install -y autoconf automake make gcc-c++ redis pcre-devel zlib-devel libmemcached-devel libevent-devel openssl-devel || { echo 'installation failed' ; exit 1; }

pushd memtier_benchmark
autoreconf -ivf
./configure
make || { echo 'compilation failed' ; exit 1; }
make install || { echo 'installation failed' ; exit 1; }
