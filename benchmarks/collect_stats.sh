#/bin/bash

DURATION=$1

echo "time,copy,reuse"

for I in $(seq 1 1 $DURATION); do
	REUSE=`cat /proc/vmstat | grep pgreuse | cut -d " " -f 2`
	COPY=`cat /proc/vmstat | grep pgcopy | cut -d " " -f 2`
	TIME=`date +%s%N`

	echo "$TIME,$COPY,$REUSE"

	sleep 1
done
