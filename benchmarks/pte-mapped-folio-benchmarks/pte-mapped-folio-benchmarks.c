/*
 * Simple benchmarks for testing performance of certain (un)map operations
 * with PTE-mapped folios.
 *
 * Copyright (C) 2024 Red Hat, Inc.
 * Author: David Hildenbrand <david@redhat.com>
 */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/mman.h>

/* We'll use 1 GiB, that should map to 512 2MiB THP on x86. */
const size_t size = 1024 * 1024 * 1024ull;
const int warmup_iterations = 10;

static size_t pagesize;
static size_t pmdsize;
static size_t thpsize;
static int pagemap_fd;

static size_t read_pmdsize(void)
{
	int fd;
	char buf[20];
	ssize_t num_read;

	fd = open("/sys/kernel/mm/transparent_hugepage/hpage_pmd_size",
		  O_RDONLY);
	if (fd == -1)
		return 0;

	num_read = read(fd, buf, 19);
	if (num_read < 1) {
		close(fd);
		return 0;
	}
	buf[num_read] = '\0';
	close(fd);

	return strtoul(buf, NULL, 10);
}

static void clear_softdirty(void)
{
	int ret;
	const char *ctrl = "4";
	int fd = open("/proc/self/clear_refs", O_WRONLY);

	if (fd < 0) {
		fprintf(stderr, "opening clear_refs failed\n");
		exit(1);
	}
	ret = write(fd, ctrl, strlen(ctrl));
	close(fd);
	if (ret != (int)strlen(ctrl)) {
		fprintf(stderr, "writing clear_refs failed\n");
		exit(1);
	}
}

static uint64_t pagemap_get_entry(int fd, char *start)
{
	const unsigned long pfn = (unsigned long)start / getpagesize();
	uint64_t entry;
	int ret;

	ret = pread(fd, &entry, sizeof(entry), pfn * sizeof(entry));
	if (ret != sizeof(entry)) {
		fprintf(stderr, "ERROR: Reading pagemap failed\n");
		exit(1);
	}
	return entry;
}

static bool pagemap_is_populated(int fd, char *start)
{
	return pagemap_get_entry(fd, start) &
		((1ull << 63) | (1ull << 62));
}

enum mode {
	MODE_FORK = 0,
	MODE_MUNMAP,
	MODE_DONTNEED,
	MODE_PTE_DONTNEED,
	MODE_COW,
	MODE_COW_BYTE,
	MODE_REUSE,
	MODE_REUSE_BYTE,
	MODE_WRITE_FAULT,
	MODE_WRITE_FAULT_BYTE,
	MODE_REMAP,
	NR_MODES,
};

const char * const mode_to_str[NR_MODES] = {
	/* fork(). */
	[MODE_FORK] = "fork",
	/* munmap complete range. */
	[MODE_MUNMAP] = "munmap",
	/* MADV_DONTNEED individual PTEs. */
	[MODE_DONTNEED] = "dontneed",
	/* MADV_DONTNEED individual PTEs. */
	[MODE_PTE_DONTNEED] = "pte-dontneed",
	/* COW after fork(), writing full range. */
	[MODE_COW] = "cow",
	/* COW after fork(), writing a single byte per PTE. */
	[MODE_COW_BYTE] = "cow-byte",
	/* Reuse after fork(), writing full range. */
	[MODE_REUSE] = "reuse",
	/* Reuse after fork(), writing a single byte per PTE. */
	[MODE_REUSE_BYTE] = "reuse-byte",
	/* Reuse without fork(), writing full range. */
	[MODE_WRITE_FAULT] = "write-fault",
	/* Reuse without fork(), writing a single byte per PTE. */
	[MODE_WRITE_FAULT_BYTE] = "write-fault-byte",
	/* PTE-remap the PMD-mapped THP. */
	[MODE_REMAP] = "remap",
};

static int parse_mode(const char *str)
{
	int i;
	
	for (i = 0; i < NR_MODES; i++) {
		if (!strcmp(str, mode_to_str[i]))
			return i;
	}
	return NR_MODES;
};

static double time_diff(struct timespec start, struct timespec end)
{
	struct timespec delta;

	delta.tv_sec = end.tv_sec - start.tv_sec;
	delta.tv_nsec = end.tv_nsec - start.tv_nsec;
	if (delta.tv_sec > 0 && delta.tv_nsec < 0) {
		delta.tv_nsec += 1000000000ull;
		delta.tv_sec--;
	} else if (delta.tv_sec < 0 && delta.tv_nsec > 0) {
		delta.tv_nsec -= 1000000000ull;
		delta.tv_sec++;
	}
	return (double)delta.tv_sec + (double)delta.tv_nsec * 1.e-9;
}

static void pte_map_thps(char *mem, size_t size)
{
	size_t offs;
	int ret = 0;

	if (thpsize != pmdsize)
		return;

	/* PTE-map each THP by temporarily splitting the VMAs. */
	for (offs = 0; offs < size; offs += pmdsize) {
		ret |= madvise(mem + offs, pagesize, MADV_DONTFORK);
		ret |= madvise(mem + offs, pagesize, MADV_DOFORK);
	}

	if (ret) {
		fprintf(stderr, "ERROR: mprotect() failed\n");
		exit(1);
	}
}

static double measure_single(int mode)
{
	const size_t mmap_size = size + 2 * pmdsize;
	struct timespec start, end;
	char *mem = NULL, *mmap_mem;
	size_t offs;
	int i, ret;

	for (i = 0; i < 100; i++) {
		mmap_mem = mmap(0, mmap_size, PROT_READ|PROT_WRITE,
			   MAP_PRIVATE | MAP_ANON, -1, 0);
		if (mmap_mem == MAP_FAILED) {
			fprintf(stderr, "ERROR: mmap() failed\n");
			exit(1);
		}

		if (madvise(mmap_mem, mmap_size, MADV_NOHUGEPAGE)) {
			fprintf(stderr, "ERROR: madvise() failed\n");
			exit(1);
		}

		/* For alignment purposes, we need twice the thp size. */
		mem = (char *)(((uintptr_t)mmap_mem + pmdsize) & ~(pmdsize - 1));

		/*
		 * Allocate some memory we won't ever use to make sure we have an
		 * anon vma.
		 */
		mmap_mem[mmap_size - 1] = 0;

		if (thpsize == pagesize) {
			memset(mem, 0, size);
			break;
		}

		/* Let's actually populate THPs. */
		for (offs = 0; offs < size; offs += thpsize) {
			if (madvise(&mem[offs], thpsize, MADV_HUGEPAGE)) {
				fprintf(stderr, "ERROR: madvise() failed\n");
				exit(1);
			}

			/* Let's see if we get a THP. */
			mem[offs] = 0;

			if (!pagemap_is_populated(pagemap_fd,
						  &mem[offs + thpsize - 1])) {
				break;
			}
		}

		if (offs != size) {
			munmap(mmap_mem, mmap_size);
			mem = NULL;
			continue;
		}

		break;
	}
	if (!mem) {
		fprintf(stderr, "ERROR: Could not populate enough THPs\n");
		exit(1);
	}

	/* Prevent khugepaged from doing something stupid later. */
	if (madvise(mmap_mem, mmap_size, MADV_NOHUGEPAGE)) {
		fprintf(stderr, "ERROR: madvise() failed\n");
		exit(1);
	}

	switch (mode) {
	case MODE_REMAP:
		ret = 0;
		clock_gettime(CLOCK_MONOTONIC, &start);
		for (offs = 0; offs < size; offs += pmdsize)
			ret |= madvise(mem + offs, pagesize, MADV_DONTFORK);
		clock_gettime(CLOCK_MONOTONIC, &end);
		if (ret) {
			fprintf(stderr, "ERROR: mprotect() failed\n");
			exit(1);
		}
		break;
	case MODE_DONTNEED:
		pte_map_thps(mem, size);

		clock_gettime(CLOCK_MONOTONIC, &start);
		madvise(mem, size, MADV_DONTNEED);
		clock_gettime(CLOCK_MONOTONIC, &end);
		break;
	case MODE_PTE_DONTNEED:
		pte_map_thps(mem, size);

		clock_gettime(CLOCK_MONOTONIC, &start);
		/*
		 * Discard each individual page; the first MADV_DONTNEED
		 * of a THP will trigger PTE-mapping it.
		 */
		for (offs = 0; offs < size; offs += pagesize)
			madvise(mem + offs, pagesize, MADV_DONTNEED);
		clock_gettime(CLOCK_MONOTONIC, &end);
		break;
	case MODE_MUNMAP:
		pte_map_thps(mem, size);

		clock_gettime(CLOCK_MONOTONIC, &start);
		munmap(mem, size);
		clock_gettime(CLOCK_MONOTONIC, &end);
		break;
	case MODE_COW:
	case MODE_COW_BYTE:
		pte_map_thps(mem, size);

		/* Share the pages using COW, and keep the child alive. */
		ret = fork();
		if (ret < 0) {
			fprintf(stderr, "ERROR: fork() failed\n");
			exit(1);
		} else if (!ret) {
			pause();
			exit(0);
		}

		if (mode == MODE_COW_BYTE) {
			clock_gettime(CLOCK_MONOTONIC, &start);
			for (offs = 0; offs < size; offs += pagesize)
				mem[offs] = 1;
			clock_gettime(CLOCK_MONOTONIC, &end);
		} else {
			clock_gettime(CLOCK_MONOTONIC, &start);
			memset(mem, 1, size);
			clock_gettime(CLOCK_MONOTONIC, &end);
		}

		if (kill(ret, SIGKILL)) {
			fprintf(stderr, "ERROR: kill() failed\n");
			exit(1);
		}
		wait(&ret);
		break;
	case MODE_REUSE:
	case MODE_REUSE_BYTE:
		pte_map_thps(mem, size);

		/* Share the pages using COW, and wait until the child quit. */
		ret = fork();
		if (ret < 0) {
			fprintf(stderr, "ERROR: fork() failed\n");
			exit(1);
		} else if (!ret) {
			exit(0);
		}
		wait(&ret);

		if (mode == MODE_REUSE_BYTE) {
			clock_gettime(CLOCK_MONOTONIC, &start);
			for (offs = 0; offs < size; offs += pagesize)
				mem[offs] = 1;
			clock_gettime(CLOCK_MONOTONIC, &end);
		} else {
			clock_gettime(CLOCK_MONOTONIC, &start);
			memset(mem, 1, size);
			clock_gettime(CLOCK_MONOTONIC, &end);
		}
		break;
	case MODE_WRITE_FAULT:
	case MODE_WRITE_FAULT_BYTE:
		pte_map_thps(mem, size);

		/*
		 * wrprotect all pages, and stop mprotect() from fixing
		 * it up by simply enabling softdirty tracking. PAE is
		 * set because there was no fork, so this write fault should
		 * be extremely fast.
		 */
		ret = mprotect(mem, size, PROT_READ);
		clear_softdirty();
		ret |= mprotect(mem, size, PROT_READ | PROT_WRITE);
		if (ret) {
			fprintf(stderr, "ERROR: mprotect() failed\n");
			exit(1);
		}

		if (mode == MODE_WRITE_FAULT_BYTE) {
			clock_gettime(CLOCK_MONOTONIC, &start);
			for (offs = 0; offs < size; offs += pagesize)
				mem[offs] = 1;
			clock_gettime(CLOCK_MONOTONIC, &end);
		} else {
			clock_gettime(CLOCK_MONOTONIC, &start);
			memset(mem, 1, size);
			clock_gettime(CLOCK_MONOTONIC, &end);
		}

		break;
	case MODE_FORK:
		pte_map_thps(mem, size);

		clock_gettime(CLOCK_MONOTONIC, &start);
		ret = fork();
		clock_gettime(CLOCK_MONOTONIC, &end);
		if (ret < 0) {
			fprintf(stderr, "ERROR: fork() failed\n");
			exit(1);
		} else if (!ret) {
			exit(0);
		}
		break;
	default:
		abort();
	}

	munmap(mmap_mem, mmap_size);

	return time_diff(start, end);
}

int main(int argc, char *argv[])
{
	int mode, i, order, seconds;
	time_t start;

	pagesize = getpagesize();
	pmdsize = read_pmdsize();
	if (!pmdsize) {
		fprintf(stderr, "ERROR: Could not detect PMD THP size\n");
		return 1;
	}
	pagemap_fd = open("/proc/self/pagemap", O_RDONLY);
	if (pagemap_fd < 0) {
		fprintf(stderr, "ERROR: Opening pagemap failed\n");
		return 1;
	}

	if (argc != 4) {
		fprintf(stderr, "Usage: %s $ORDER $MODE $SECONDS\n", argv[0]);
		return 1;
	}
	order = strtoul(argv[1], NULL, 10);
	if (order == 1 || order > 31 || pagesize * (1ul << order) > pmdsize) {
		fprintf(stderr, "ERROR: Unsupported order '%s'\n", argv[1]);
		return 1;
	}
	thpsize = pagesize * (1ul << order);

	mode = parse_mode(argv[2]);
	if (mode < 0 || mode >= NR_MODES) {
		fprintf(stderr, "ERROR: Unknown mode '%s'\n", argv[2]);
		return 1;
	}
	if (mode == MODE_REMAP && thpsize != pmdsize) {
		fprintf(stderr, "ERROR: Incompatible mode '%s'\n", argv[2]);
		return 1;
	}

	seconds = strtol(argv[3], NULL, 10);
	if (seconds < 1) {
		fprintf(stderr, "ERROR: Seconds has to be > 0\n");
		return 1;
	}

	for (i = 0; i < warmup_iterations; i++) {
		double duration = measure_single(mode);

		asm volatile("" : "+r" (duration));
	}

	start = time(NULL);
	do {
		double duration = measure_single(mode);

		printf("%lf\n", duration);
	} while (time(NULL) < start + seconds);

	return 0;
}
