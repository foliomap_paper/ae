#/bin/bash

DIR=$1
TIME=60

echo "Running memtier_benchmark for $TIME seconds"

STDERR=`mktemp`
STDOUT=`mktemp`
TMP=`mktemp`
TMP2=`mktemp`
JSON=`mktemp`
STATS=`mktemp`

#start collecting stats
./collect_stats.sh $((TIME + 10)) > $STATS &
PID=$!

OUT=`memtier_benchmark --test-time=$TIME -c3 --key-pattern=P:P --key-maximum=100000000 --pipeline=2000 --hide-histogram --json-out-file=$JSON 2>$STDERR > $STDOUT`
wait $PID

sed -e 's/\r/\n/g' -i $STDERR
tail -n +5 $STDERR | head -n -4 > $TMP

echo "Sec,TotalOps,CurOps/s,AvgOps/s,CurMb/s,AvgMb/s,CurLatency(ms),AvgLatency(ms)" > $DIR/results.csv
cat $TMP | tr -s " " | tr -d "()MB/sec," | cut --output-delimiter "," -d " " -f 4,8,10,12,14,16,17,19 >> $DIR/results.csv

echo "Copies/s" > $TMP
tail -n +1 $STATS | cut -d "," -f 2 | awk 'p{print $0-p}{p=$0}' | tail -n +2 | head -n 61 >> $TMP

echo "Sec" > $TMP2
seq 0 60 >> $TMP2

paste -d , $TMP2 $TMP > $DIR/copies.csv

redis-cli -h 127.0.0.1 -p 6379 info > $DIR/redis
grep "latest_fork_usec" $DIR/redis | cut -d ":" -f2 > $DIR/fork_usec

cp $STDERR $DIR/stderr
cp $STDOUT $DIR/stdout
cp $STATS $DIR/stats.csv
cp $JSON $DIR/results.json

END_TIME=`cat $JSON | grep "Finish time" | tr -d " \t" | cut -d ":" -f2`
END_TIME=$((END_TIME / 1000))

echo $END_TIME > $DIR/end_time

rm $STDERR $TMP $TMP2 $JSON $STATS $STDOUT
