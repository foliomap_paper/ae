import multiprocessing as mp
import numpy

size = pow(512, 3)
arr = numpy.ones(size)

def fn(range):
    return numpy.sum(arr[range[0]:range[1]])

def multi_process_sum(arr):
    c = int(size / 8) 
    ranges = [(i,i + c) for i in range(0, size, c)] 

    pool = mp.Pool(processes = 8)
    return int(sum(pool.map(fn, ranges)))

assert(multi_process_sum(arr) == size)
arr[0:size] = 0
assert(multi_process_sum(arr) == 0)
