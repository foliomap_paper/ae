#!/bin/bash

# redis.conf
#
# * Disable automatic snapshots
#   -> save ""
# * Make loading/saving a bit faster
#   -> rdbcompression no
# * Allow THP
#   -> disable-thp no
#
# /etc/systemd/system/redis.service.d/limit.conf
#   -> increase timeouts

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if ! command -v memtier_benchmark &> /dev/null; then
        echo "memtier_benchmark not installed"
        exit 1
fi

if ! command -v redis-cli &> /dev/null; then
        echo "redis not installed"
        exit 1
fi

./prepare.sh

prepare() {
        if [ -f "/var/lib/redis/orig.rdb" ]; then
                return
        fi

        echo "Preparing redis"

	# Make a backup just in case
        if [ -f "/var/lib/redis/dump.rdb" ]; then
                mv /var/lib/redis/dump.rdb /var/lib/redis/backup.rdb
        fi

	# Create our DB and move it to another location
        systemctl stop redis
        systemctl start redis

	echo "Creating DB"
        memtier_benchmark --test-time=300 --key-maximum=100000000 --pipeline=2000 --ratio=1:0 --key-pattern=P:P > /dev/null 2>&1
	echo "Saving DB to disk"
        redis-cli -h 127.0.0.1 -p 6379 shutdown save now > /dev/null
        systemctl stop redis
	mv /var/lib/redis/dump.rdb /var/lib/redis/orig.rdb
}

run_once() {
	DIR=$1

        # Load our DB
	echo "Loading DB"
        systemctl stop redis
	rm /var/lib/redis/dump.rdb > /dev/null
	cp /var/lib/redis/orig.rdb /var/lib/redis/dump.rdb
        systemctl start redis

        # trigger a background snapshot
	echo "Triggering background snapshot"
        redis-cli -h 127.0.0.1 -p 6379 BGSAVE

        # wait until it is done
        sleep 10

        REM=`redis-cli -h 127.0.0.1 -p 6379 info | grep current_fork_perc | cut -d":" -f2 | tr -d ' \n\r'`
        while [[ "$REM" != "0.00" ]]; do
                sleep 1
                REM=`redis-cli -h 127.0.0.1 -p 6379 info | grep current_fork_perc | cut -d":" -f2 | tr -d ' \n\r'`
        done;

	./memtier.sh $DIR

        systemctl stop redis
}

# See if we have to create the database
prepare

KERNEL=`uname -r`
OUTDIR="results/memtier/$KERNEL"
if [ -d $OUTDIR ]; then
        echo "$OUTDIR already exist"
        exit 1
fi
mkdir -p $OUTDIR

# 4 KiB results
echo "Processing 4 KiB"
for I in $(seq 1 1 10); do
	echo "Perforing run $I out of 10"

	CURDIR=$OUTDIR/$I/4KiB
	mkdir -p $CURDIR
	run_once $CURDIR
done

# 64 KiB results
echo "Processing 64 KiB"
echo "always" > /sys/kernel/mm/transparent_hugepage/hugepages-64kB/enabled

for I in $(seq 1 1 10); do
	echo "Perforing run $I out of 10"

	CURDIR=$OUTDIR/$I/64KiB
	mkdir -p $CURDIR
	run_once $CURDIR
done
