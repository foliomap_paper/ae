#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# make sure it is compiled
make -C write-fault-latency > /dev/null || { echo 'compilation failed' ; exit 1; }

./prepare.sh

KERNEL=`uname -r`
DIR=`pwd`/results/write-fault-latency/$KERNEL

if [ -d "$DIR" ]; then
        echo "Results already exist"
        exit 1
fi
mkdir -p $DIR

pushd write-fault-latency

MODES="cow reuse"
ORDERS="0 2 3 4 5 6 7 8 9"

echo never > /sys/kernel/mm/transparent_hugepage/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-16kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-32kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-64kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-128kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-256kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-512kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-1024kB/enabled || { echo 'enabling THP failed' ; exit 1; }

echo madvise > /sys/kernel/mm/transparent_hugepage/hugepages-2048kB/enabled || { echo 'enabling THP failed' ; exit 1; }


for M in $MODES; do
	echo "FolioSize(KiB),Time(ns),RSD" > $DIR/$M.csv
done

for O in $ORDERS; do
	foliosize=$((4 * (1 << $O)))
	echo "Processing ${foliosize} KiB (order-$O)"

	echo "Op,Time(ns),RSD" > $DIR/order-$O.csv

	for M in $MODES; do
		echo "Processing $M"

		nice -n -5 taskset -c 2 ./write-fault-latency $O $M 10 > $DIR/order-$O-$M.csv

		NUMBERS=`cat $DIR/order-$O-$M.csv`
		sd=$(
			echo "$NUMBERS" |
				awk '{sum+=$1; sumsq+=$1*$1}END{printf "%.6f", sqrt(sumsq/NR - (sum/NR)**2) }'
		)

		avg=$(
			echo "$NUMBERS" |
				awk '{ total += $1; count++ } END { printf "%.6f", total/count }'
		)

		rsd=`echo "scale=6 ; $sd / $avg" | bc`

		echo "$M,$avg,$rsd"
		echo "$M,$avg,$rsd" >> $DIR/order-$O.csv
		echo "$foliosize,$avg,$rsd" >> $DIR/$M.csv
	done
done

