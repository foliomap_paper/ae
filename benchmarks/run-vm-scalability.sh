#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# make sure it is compiled
make -C vm-scalability > /dev/null || { echo 'compilation failed' ; exit 1; }

./prepare.sh

TASKS=`nproc --all`
CASES="case-anon-cow-seq case-anon-cow-rand"
SIZES="4 16 32 64 128 256 512 1024 2048"
KERNEL=`uname -r`
DIR=`pwd`/results/vm-scalability/$KERNEL

run_case() {
	CASE=$1
	SIZE=$2
	DIR=$3

	sync; echo 3 > /proc/sys/vm/drop_caches

	pushd vm-scalability

	# warmup
	./usemem --runtime 3000 -n $TASKS --prealloc --prefault 1g > /dev/null

	echo "Bytes,Usecs,KBs" >> $DIR/$s-$c.csv

	for i in $(seq 1 1 10); do

		export nr_task=$TASKS
		export unit_size=$((1024*1024*1024*TASKS))

		bash -c "./$c" > $DIR/$c-tmp
		ret=$?
		if [ $ret -ne 0 ]; then
			echo "$c failed with $ret"
		fi

		bytes_total=0
		usecs_total=0
		kbs_total=0
		while read -r line; do
			bytes=`echo $line | cut -d" " -f1`
			usecs=`echo $line | cut -d" " -f4`
			kbs=`echo $line | cut -d" " -f7`
			bytes_total=$((bytes_total+bytes))
			usecs_total=$((usecs_total+usecs))
			kbs_total=$((kbs_total+kbs))
		done < $DIR/$c-tmp
		rm $DIR/$c-tmp

		echo "$bytes_total,$usecs_total,$kbs_total" >> $DIR/$s-$c.csv
	done

	NUMBERS=`tail -n +2 $DIR/$s-$c.csv | cut -d"," -f3`

	gbs_sd=$(
	    echo "$NUMBERS" |
	        awk '{sum+=$1; sumsq+=$1*$1}END{printf "%.6f", sqrt(sumsq/NR - (sum/NR)**2)/1024/1024}'
	)
	gbs_average=$(
	    echo "$NUMBERS" |
	        awk '{ total += $1; count++ } END { printf "%.6f", total/count/1024/1024 }'
	)
	gbs_rsd=`echo "scale=6 ; $gbs_sd / $gbs_average" | bc`

	echo "$s,$gbs_average,$gbs_rsd"
	echo "$s,$gbs_average,$gbs_rsd" >> $DIR/$c.csv

	popd
}

if [ -d "$DIR" ]; then
	echo "Results already exist"
	exit 1
fi
mkdir -p $DIR


for c in $CASES; do
	echo "Processing case $c"

	echo "FolioSize(KiB),Throughput(GB/s),RSD" > $DIR/$c.csv

	for s in $SIZES; do
		echo "Processing size $s KiB"

		if [[ "$s" != "4" ]]; then
			echo "Enabling $s"
			echo always > /sys/kernel/mm/transparent_hugepage/hugepages-${s}kB/enabled || { echo 'enabling THP failed' ; exit 1; }
		fi

		run_case $c $s $DIR

		if [[ "$s" != "4" ]]; then
			echo "Disabling $s"
			echo never > /sys/kernel/mm/transparent_hugepage/hugepages-${s}kB/enabled || { echo 'disabling THP failed' ; exit 1; }
		fi
	done
done
